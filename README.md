# Dockerbuild 

OCP4.6 도커빌드 관련 샘플 리소스
# 

## https://docs.openshift.com/container-platform/4.6/builds/understanding-image-builds.html#builds-strategy-docker-build_understanding-image-builds
## https://docs.openshift.com/container-platform/4.6/builds/creating-build-inputs.html#builds-define-build-inputs_creating-build-inputs

### Build inputs > Git repositories
```
$ oc process -f https://gitlab.com/injeinc-cnp/dockerbuild/-/raw/master/eap73-openjdk11-basic-docker.yaml APPLICATION_NAME=eap-app01 SOURCE_REPOSITORY_URL=https://gitlab.com/injeinc-cnp/dockerbuild.git -o yaml | oc create -f -
```

### Build inputs > Inline Dockerfile definitions + External artifacts
```
$ oc process -f https://gitlab.com/injeinc-cnp/dockerbuild/-/raw/master/eap73-openjdk11-basic-docker-inline.yaml APPLICATION_NAME=eap-app02 BUILD_ARTIFACT_URL=https://gitlab.com/injeinc-cnp/dockerbuild/-/raw/master/ROOT.war -o yaml | oc create -f -
```
